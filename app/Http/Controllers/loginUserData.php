<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\user_info;
use Session; 

class loginUserData extends Controller
{
    //
    function onloadData()
    {
    	$user_info = user_info::where('email',Session::get('LoginUserEmail'))->first();
    	if(isset($user_info))
    	{
    		Session::put('LoginUserName', $user_info->firstName . " " .$user_info->lastName );
    		Session::put('LoginUserGender', $user_info->gender);

    		$fgender = 0;
    		$fagefrom = 1;
    		$fageto = 100;
    		if($user_info->gender == 0)
    		{
    			$fgender = 1;
    			$t = strtotime($user_info->dob);
    			$fageto = $user_info->dob;
    			$fagefrom = date('Y-m-d', strtotime($fageto. ' - 3 years'));//strtotime('-1995 days', $fageto);
    		}
    		else
    		{
    			$fgender = 0;
    			$t = strtotime($user_info->dob);
    			$fagefrom = $user_info->dob;
    			$fageto = date('Y-m-d', strtotime($fagefrom. ' + 3 years'));;//strtotime('+1995 days', $t);   
    			//$fageto = date("Y-m-d", strtotime('+ 1995 day' , strtotime($t))); 			
    		}

    		$user_info = (user_info::where('gender',$fgender)->whereBetween('dob',[$fagefrom,$fageto])->get());
    		
    		//dd($user_info[0]->firstName);
    		return view('dashboard', ["suggestions"=>$user_info]);



    		return view('/dashboard');
    	}
    	else
    	{
    		return redirect('/login')->with('status', 'Session Expired');
    	}
    	//return null;
    }

    function checkuserlogin(Request $request)
    {
    	$user_info = user_info::where('email',$request->email)->where('password',md5($request->password))->first();

    	if(isset($user_info))
    	{
    		Session::put('LoginUserName', $user_info->firstName . " " .$user_info->lastName );
    		Session::put('LoginUserGender', $user_info->gender);
    		Session::put('LoginUserEmail', $user_info->email);
    		return redirect('/dashboard');
    	}
    	else
    	{
    		return redirect('/login')->with('status', 'Session Expired');
    	}
    }

    function logout()
    {

    	Session::put('LoginUserEmail', null );
    	Session::put('LoginUserName', null );
    	Session::put('LoginUserName', null );
    	return redirect('/login');
    	
    }
}
