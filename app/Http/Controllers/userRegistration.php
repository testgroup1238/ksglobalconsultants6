<?php

namespace App\Http\Controllers;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\user_info;
use Laravel\Socialite\Facades\Socialite;
use Session;

class userRegistration extends Controller
{
    //
    public function addUser(Request $request)
    {    	

    	$user_info = user_info::where('email',$request->email)->first();
    	if(isset($user_info))
    	{
    		return redirect('/login')->with('status', $request->email . ' this email already registred...!');
    	}
    	



    	$dob =  explode("/",$request->reservationdate);//str_replace("/","-",$request->reservationdate);
    	$dob = $dob[2]."-".$dob[0]."-".$dob[1];
    	$user_info = new user_info;
    	$user_info->firstName = $request->firstname;
    	$user_info->lastName = $request->lastname;
    	$user_info->email = $request->email;
    	$user_info->gender = $request->radio1;
    	$user_info->income = $request->income;
    	$user_info->occupation = $request->occupationselect;
    	$user_info->expectedIncome = $request->range_1;
    	$user_info->Manglik = $request->Manglik;
    	$user_info->familyType = $request->famitytypeselect;
    	$user_info->dob = $dob;
    	$user_info->password = md5($request->password1);
    	$user_info->save();

        return redirect('/login')->with('status', 'Registration Done, Please login.');
    	//return $request;//Socialite::driver('google')->scopes(['read:user', 'public_repo'])->redirect();
    }
    public function redirectToGoogle()
    {
    	return Socialite::driver('google')->redirect();
    }
    public function googleCallbackData()
    {
    	$userData = Socialite::driver('google')->user();
    	print_r($userData);

    	$user_info = user_info::where('email',$userData->email)->first();
    	if(isset($user_info))
    	{
    		Session::put('LoginUserEmail', $userData->email);
    		//Session::put('LoginUserName', $user_info->firstName . " " .$user_info->lastName );
    		//Session::put('LoginUserGender', $userData->gender);
    		return redirect('/dashboard');
    	}
    	else
    	{
    		return redirect('/login')->with('status', 'User Not Found...!');
    	}


    	die();
    }

}
