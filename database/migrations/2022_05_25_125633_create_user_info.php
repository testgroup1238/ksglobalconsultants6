<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserInfo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_info', function (Blueprint $table) {
            $table->BigInteger('user_id')->length(20)->unsigned();
            $table->string('firstName')->length(100);
            $table->string('lastName')->length(100);
            $table->string('email')->length(50);
            $table->string('password')->length(50);
            $table->tinyInteger('gender')->length(4);
            $table->date('dob');
            $table->float('income');
            $table->float('expectedIncome');
            $table->string('occupation')->length(100);
            $table->string('familyType')->length(50);
            $table->tinyInteger('Manglik')->length(4);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_info');
    }
}
