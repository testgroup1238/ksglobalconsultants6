<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Faker\Factory;
use Illuminate\Support\Arr;
use Illuminate\Database\Carbon;

class addUser extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	for($i=0;$i<10;$i++)
    	{
	    	$gender1 = ["0", "1"];
	    	$numberrand = [0,1,2,3,4,5,6,7,8,9,10];
	    	$numberrand = array_rand($numberrand);
	    	$date1 = date('Y-m-d');
	    	$date1 = date('Y-m-d', strtotime($date1. ' + '.$numberrand.' years'));

	    	$occupation = ['Government Job', 'Private job', 'Business'];
	    	$expectedIncome = ['5,10', '6,9', '25,30'];
	    	$familyType = ['Joint family', 'Nuclear family'];
	    	$Manglik =['1', '0'];
	    	$income =[1000, 2000,3000,4000,5000,6000,7000];

	        DB::table('user_infos')->insert([
	        	'firstName'=>Str::random(5),
	        	'lastName'=>Str::random(5),
	        	'password'=> md5(111),
	        	'income'=>$income[array_rand($income)],
	        	'gender'=>$gender1[array_rand($gender1)],
	        	'dob'=>$date1,
	        	'email'=>Str::random(10)."@gmail.com",
	        	'expectedIncome'=>$expectedIncome[array_rand($expectedIncome)],
	        	'occupation'=>$occupation[array_rand($occupation)],
	        	'familyType'=>$familyType[array_rand($familyType)],
	        	'Manglik'=>$Manglik[array_rand($Manglik)]
	        ]);    
   		 }
    }
}
