<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\userRegistration;
use App\Http\Controllers\loginUserData;
use Laravel\Socialite\Facades\Socialite;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('userRegistration');
});
Route::post('register', [userRegistration::class, 'addUser']);


Route::get('/auth/redirect', function () {
    return Socialite::driver('google')->redirect();
});
 
Route::get('/auth/callback', function () {
    $user = Socialite::driver('google')->user();
});
Route::get('login/google/callback', function () {
    $user = Socialite::driver('google')->user();
});

Route::view('/login','login');
Route::get('/google',[userRegistration::class,"redirectToGoogle"]);
Route::get('/login/google/callback',[userRegistration::class,"googleCallbackData"]);
Route::post('/weblogin',[loginUserData::class,"checkuserlogin"]);




Route::group(['middleware'=>['loginUserCheck']], function(){
//Route::view('/dashboard','dashboard');
	Route::get('/dashboard',[loginUserData::class,"onloadData"]);
	Route::get('/logout',[loginUserData::class,"logout"]);

});